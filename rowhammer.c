#include "rowhammer.h"


DRAM_t DRAM;
PTE_t PT[MAPPING_SIZE/PAGE_SIZE];
PTE_t fake_PT[SIZE + 1];
int limit = MAPPING_SIZE/PAGE_SIZE;
uint64_t sbdr[SBDR_NUM];

uint64_t GetPageFrameNumber(int pagemap, uint8_t* virtual_address) {
    // Read the entry in the pagemap.
    uint64_t value;
    int got = pread(pagemap, &value, 8,
            ((uintptr_t)(virtual_address) / 0x1000) * 8);
    assert(got == 8);
    uint64_t page_frame_number = value & ((1ULL << 54)-1);
    return page_frame_number;
}

void init_DRAM()
{
    int b, r, p;

    for(b=0; b<BANK_NUM; b++)
        for(r=0; r<ROW_NUM; r++)
            for(p=0; p<PAGE_NUM; p++){
                DRAM.bank[b].row[r].page[p].virtual_address = 0;
                DRAM.bank[b].row[r].page[p].physical_address = 0;
            }
}

int extract_bank(uint64_t page_frame_number)
{
}
int extract_row(uint64_t page_frame_number)
{
}

void make_DRAM()
{
    init_DRAM();
    int pagemap = open("/proc/self/pagemap", O_RDONLY);
    void *virtual_address = mmap(NULL, MAPPING_SIZE, PROT_READ | PROT_WRITE, MAP_POPULATE | MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);


    void *ptr;
    for(ptr = virtual_address; ptr < virtual_address+MAPPING_SIZE; ptr = ptr + 1024){
        uint64_t page_frame_number = GetPageFrameNumber(pagemap, ptr);

    }
}

void* make_PT()
{
    int pagemap = open("/proc/self/pagemap", O_RDONLY);
    void *virtual_address = mmap(NULL, MAPPING_SIZE, PROT_READ | PROT_WRITE, MAP_POPULATE | MAP_ANONYMOUS | MAP_PRIVATE, -1, 0);


    void *ptr = virtual_address;
    int i;
    for(i=0; i<limit; i++)
    {
        PT[i].va = virtual_address;
        uint64_t pfn = GetPageFrameNumber(pagemap, virtual_address);
        PT[i].pfn = pfn;
        virtual_address = virtual_address + PAGE_SIZE;
    }
    return ptr;
}
void make_fake_PT(int index)
{
    int i;
    for(i=0; i<SIZE + 1; i++)
    {
        fake_PT[i].va = PT[i+index].va;
        fake_PT[i].pfn = PT[i+index].pfn;
        //        printf("pfn : %"PRIx64"\n", fake_PT[i].pfn);
    }
    return ;
}
void clean_PT(void* ptr)
{
    munmap(ptr, MAPPING_SIZE);
    return ;
}

int get_cont_index()
{
    int i;
    uint64_t pfn = PT[0].pfn;
    int cont = 1;
    int c=-1;
    printf("cont\n");
    for(i=1; i<limit; i++)
    {

        if((PT[i].pfn - pfn) == 1){
            cont++;
            if(cont > SIZE * 2){
                if(c == 1)
                    break;
                else{
                    c++;
                    cont = 0;
                }

               // break;
            }
        }
        else{

//            printf("1. %"PRIx64", 2. %"PRIx64", cont : %d\n",pfn, PT[i].pfn, cont);
            cont = 0;
            
        }
        pfn = PT[i].pfn;
    }
    printf("cont: %d\n",cont);
    if(i == limit)
        return -1;

    printf("E\n");
    int index = i - SIZE * 2;
    for(i=index; i <index + SIZE * 2; i++){
        if((PT[i].pfn & ~(0b1111111111)) == PT[i].pfn)
            break;
    }
    printf("F\n");
    index = i;

    return index;
}

void get_SBDR(int index)
{
    int i, j = 0;
    int latency, latency_ = 0;
    int avg = 0;
    int num = 0;

    for(i=1; i<SIZE + 1; i++)
    {
        latency_ = check_latency(fake_PT[0].va, fake_PT[i].va);
        latency = latency + latency_;
//        printf("%d. latency : %d, pfn : %"PRIx64" / %"PRIx64"\n", i+1, latency_, fake_PT[0].pfn, fake_PT[i].pfn);

    }
    avg = latency / i;
    sbdr[j++] = fake_PT[0].pfn;
    for(i=1; i<SIZE + 1; i++)
    {
        latency = check_latency(fake_PT[0].va, fake_PT[i].va);

        if(latency > avg + 60)
        {
            sbdr[j++] = fake_PT[i].pfn;
            if(j > SBDR_NUM)
                break;
        }

    }

    return ;

}

void calculate(int par_num, uint64_t potential_bit)
{

}

uint64_t time_stamp()
{
    unsigned hi, lo;
    asm volatile(
            "cpuid;"
            "rdtsc;"
            : "=a"(lo), "=d"(hi)

            );
    return ((unsigned long long)lo)|(((unsigned long long)hi)<<32);
}


int check_latency(void* VA1, void* VA2)
{
    int i;
    uint64_t start, end;
    uint64_t latency = 0;
    int j=0;
    for(i=0; i<1000; i++)
    {
        start = time_stamp();
        asm volatile
            (
             "mov (%0), %%r8;"
             "mov (%1), %%r9;"
             "clflush (%0);"
             "clflush (%1);"
             "mfence;"
             :: "q"(VA1), "p"(VA2)
            );
        end = time_stamp();

        if(end-start < 1000){
            latency = latency + (end-start);
            j++;
        }

    }

    return latency/j;
}



void quickSort(int l, int r)
{
    int j;

    if( l < r ) 
    {
        // divide and conquer
        j = partition(l, r);
        quickSort(l, j-1);
        quickSort(j+1, r);
    }

}



int partition(int l, int r) {
    uint64_t pivot, i, j;
    uint64_t t;
    void* p;
    pivot = PT[l].pfn;
    i = l; j = r+1;

    while( 1)
    {
        do ++i; while( PT[i].pfn <= pivot && i <= r );
        do --j; while( PT[j].pfn > pivot );
        if( i >= j ) break;
         t = PT[i].pfn; PT[i].pfn = PT[j].pfn; PT[j].pfn = t;
         p = PT[i].va; PT[i].va = PT[j].va; PT[j].va = p;
    }
    t = PT[l].pfn; PT[l].pfn = PT[j].pfn; PT[j].pfn = t;
    p = PT[l].va; PT[l].va = PT[j].va; PT[j].va = p;

    return j;
}




int main()
{
    FILE *f;
    int i,j;
    f = fopen("log.txt", "wa");

    void *ptr = make_PT();

    quickSort(0, limit-1);
//    for(i=0; i<limit; i++){
//    printf("pfn : %"PRIx64"\n", PT[i].pfn);}

    int ii = get_cont_index();
    printf("index : %d, ptr : %"PRIx64"\n", ii, PT[ii].pfn);

    make_fake_PT(ii);
    int pagemap = open("/proc/self/pagemap", O_RDONLY);


    uint64_t VA1 = (uint64_t)fake_PT[0].va;
    uint64_t PA1 = GetPageFrameNumber(pagemap, fake_PT[0].va)<<12;

    for(i=0; i< SIZE + 1;i++){
        for(j=0; j< 128; j++){
            uint64_t VA2 = (uint64_t)fake_PT[i].va + 32*j;
            uint64_t PA2 = (GetPageFrameNumber(pagemap, fake_PT[i].va + 32*j)<<12) | (VA2&0xFFF);
            fprintf(f, "VA1: %"PRIx64" VA2: %"PRIx64" PA1: %"PRIx64" PA2: %"PRIx64" latency: %d\n",
                    VA1, VA2, PA1, PA2, check_latency((void *)VA1, (void *)VA2));
        }
    }




    get_SBDR(1);

    for(i=0; i<SBDR_NUM; i++)
        if(sbdr[i] != 0)
            printf("same pfn : %"PRIx64"\n", sbdr[i]);

    clean_PT(ptr);

    fclose(f);
    return 0;
}
