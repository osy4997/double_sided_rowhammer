#include <asm/unistd.h>
#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <inttypes.h>
#include <linux/kernel-page-flags.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/sysinfo.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define PAGE_NUM 128
#define ROW_NUM 1024
#define BANK_NUM 8
#define MAPPING_SIZE 1024*1024*1024
#define PAGE_SIZE 4096
#define SBDR_NUM 100
#define SIZE 4096

typedef struct page_t{
        void* virtual_address;
            uint64_t physical_address;
} page_t;

typedef struct row_t{
        page_t page[PAGE_NUM];
} row_t;


typedef struct bank_t{
        row_t row[ROW_NUM];
} bank_t;

typedef struct DRAM_t{
        bank_t bank[BANK_NUM];
}DRAM_t;

typedef struct PTE{
        void* va;
            uint64_t pfn;
}PTE_t;



uint64_t GetPageFrameNumber(int pagemap, uint8_t* virtual_address);
void make_DRAM();
void init_DRAM();
int extract_bank(uint64_t page_frame_number);
int extract_row(uint64_t page_frame_number);
void* make_PT();
void clean_PT(void *ptr);
int get_cont_index();
void make_fake_PT(int index);
int check_latency(void* VA1, void* VA2);
uint64_t time_stamp();
void get_SBDR(int index);
void quickSort(int l, int r);
int partition(int l, int r);

/*
#define PAGE_NUM 128
#define ROW_NUM 1024
#define BANK_NUM 8
#define MAPPING_SIZE 1024*1024*1024
#define PAGE_SIZE 4096
#define SBDR_NUM 100


uint64_t GetPageFrameNumber(int pagemap, uint8_t* virtual_address);
void make_DRAM();
void init_DRAM();
int extract_bank(uint64_t page_frame_number);
int extract_row(uint64_t page_frame_number);
void* make_PT();
void clean_PT(void *ptr);
int get_cont_index(int a);
void make_fake_PT(int index, int a);
int check_latency(void* VA1, void* VA2);
uint64_t time_stamp();
void get_SBDR(int index, int a);
void quickSort(int l, int r);
int partition(int l, int r);*/
